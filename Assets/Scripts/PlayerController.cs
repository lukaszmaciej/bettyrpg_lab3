﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator anim;
    Vector2 movement;
    public float moveSpeed = 2.0f;
    float dirX, dirY;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void GetInput()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");
    }
    void Animate()
    {
        if(movement.magnitude>0)
        { 
        anim.SetFloat("X", movement.x);
        anim.SetFloat("Y", movement.y);
        anim.SetFloat("Velocity", movement.magnitude);
        }

    }
    void Move()
    {
        transform.position = new Vector2(movement.x * moveSpeed * Time.deltaTime + transform.position.x, movement.y * moveSpeed * Time.deltaTime + transform.position.y);
    }
    void Update()
    {
        GetInput();
        Animate();
        Move();
    }
}
